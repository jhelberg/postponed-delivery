package main

import (
     "bufio"
     "fmt"
     "os"
     "os/exec"
     "log/syslog"
     "io"
     "time"
     "strings"
     "github.com/Code-Hex/exit"
)
// use -t for testing, other parameters will start sendmail
func main() {
  var cmd *exec.Cmd = nil         // declare cmd as it is needed for Start() later
  var headerfields []string = nil // buffering the header fields
  var newstdout io.WriteCloser    // one can only close via this interface

  logger, err := syslog.New( syslog.LOG_MAIL, "x-deliver-at" )
  if err != nil {
    os.Exit( exit.UNAVAILABLE )
  } else {
    defer logger.Close()
  }

  reader := bufio.NewReader( os.Stdin )
  writer := bufio.NewWriter( os.Stdout )
  if len( os.Args ) > 1 && os.Args[ 1 ] != "-t"  {
    if logger != nil {
      logger.Info( fmt.Sprintf( "preparing sendmail with %s", 
                       append( []string{"-G", "-i"}, os.Args[1:]... )) )
    }
    cmd = exec.Command( "/usr/sbin/sendmail", 
                        append( []string{"-G", "-i"}, os.Args[1:]... )... )
    newstdout, err = cmd.StdinPipe()
    if err != nil {
      if logger != nil {
        logger.Err( fmt.Sprintf( "failed sendmail pipe [%s]", err ) ) 
      }
      os.Exit( exit.UNAVAILABLE )
    }
    if logger != nil {
      logger.Info( fmt.Sprintf( "pipe to sendmail [%s]", newstdout ) )
    }
    writer = bufio.NewWriter( newstdout );
  } else { // we're testing
    logger = nil
  }

  inbody := false

  for {
    line, readerr := reader.ReadString('\n')
    if line == "\n" && inbody == false  {
        inbody = true
        if cmd != nil {
          if err := cmd.Start(); err != nil {
            if logger != nil {
              logger.Err( fmt.Sprintf( "failed to start sendmail [%s]", err ) ) 
            }
            os.Exit( exit.UNAVAILABLE )
          }
        }
        for _, elt := range headerfields {
          writer.WriteString( elt )
        }
        headerfields = nil
        if logger != nil {
          logger.Info( "flushed header fields" ) 
        }
    }
    if inbody {
      if line != "" {
         writer.WriteString( line )
      }
    } else {
      headerfields = append( headerfields, line )
    }
    if readerr != nil && readerr == io.EOF {
       break;
    }
    if inbody {
       continue
    }
    if headerfield := strings.SplitN( line, ":", 2 ); strings.ToLower( headerfield[0] ) == "x-deliver-at" {
       deliverat_body := strings.TrimSpace( headerfield[1] )
       deliverat, err := time.Parse( time.RFC1123Z, deliverat_body )
       if err != nil {
         if logger != nil {
           logger.Warning( fmt.Sprintf( "Failed to parse [%s]", deliverat_body ) )
         }
         if logger != nil {
           logger.Err( fmt.Sprintf( "Failing with DATAERROR for line [%s]", line ) )
         }
         os.Exit( exit.DATAERR );
       }
       if now := time.Now(); now.After( deliverat ) {
         continue;
       } else {
         if logger != nil {
           logger.Notice( fmt.Sprintf( "Postponing delivery based on header [%s]", line ) )
         }
         os.Exit( exit.TEMPFAIL );
       }
    }
  }

  if logger != nil {
    logger.Info( "flushing output" )
  }
  writer.Flush()
  if logger != nil { 
    logger.Info( fmt.Sprintf( "closing output %s", newstdout ) ) 
  }
  if newstdout != nil { newstdout.Close() }
  if cmd != nil {
    if err := cmd.Wait(); err != nil { 
      if logger != nil {
        logger.Err( fmt.Sprintf( "waiting for sendmail failed [%s]", err ) ) 
      }
      os.Exit( exit.UNAVAILABLE ) 
    }
  }
  os.Exit( exit.OK );
}
